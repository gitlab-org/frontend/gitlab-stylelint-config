module.exports = {
  plugins: [
    'stylelint-scss',
    'stylelint-declaration-strict-value',
    './plugins/tailwind/disallow-max-width-media-query.mjs',
  ],
  customSyntax: 'postcss-scss',
  rules: {
    'at-rule-disallowed-list': ['debug', 'extend'],
    'at-rule-no-unknown': null,
    'at-rule-no-vendor-prefix': true,
    'block-no-empty': true,
    'color-hex-length': 'short',
    'color-named': [
      'never',
      {
        ignore: ['inside-function'],
      },
    ],
    'color-no-invalid-hex': true,
    'declaration-block-no-duplicate-properties': [
      true,
      {
        ignore: ['consecutive-duplicates'],
      },
    ],
    'declaration-block-single-line-max-declarations': 1,
    'declaration-property-value-disallowed-list': {
      border: ['none'],
      'border-top': ['none'],
      'border-right': ['none'],
      'border-bottom': ['none'],
      'border-left': ['none'],
    },
    'function-url-quotes': 'always',
    'length-zero-no-unit': true,
    'max-nesting-depth': [
      3,
      {
        ignoreAtRules: ['each', 'media', 'supports', 'include'],
        severity: 'warning',
      },
    ],
    'media-feature-name-no-vendor-prefix': true,
    'property-no-unknown': true,
    'property-no-vendor-prefix': [true, { ignoreProperties: ['user-select'] }],
    'rule-empty-line-before': [
      'always-multi-line',
      {
        except: ['first-nested'],
        ignore: ['after-comment'],
      },
    ],
    'scale-unlimited/declaration-strict-value': [
      '/color$/',
      {
        ignoreValues: ['inherit', 'transparent', 'currentColor', 'initial'],
      },
    ],
    'scss/at-extend-no-missing-placeholder': [true, { severity: 'warning' }],
    'scss/at-function-pattern': '^[a-z]+([a-z0-9-]+[a-z0-9]+)?$',
    'scss/load-no-partial-leading-underscore': true,
    'scss/at-import-partial-extension-blacklist': ['scss'],
    'scss/at-mixin-pattern': '^[a-z]+([a-z0-9-]+[a-z0-9]+)?$',
    'scss/at-rule-no-unknown': true,
    'scss/dollar-variable-colon-space-after': 'always',
    'scss/dollar-variable-colon-space-before': 'never',
    'scss/dollar-variable-pattern': '^[_]?[a-z]+([a-z0-9-]+[a-z0-9]+)?$',
    'scss/no-duplicate-mixins': true,
    'scss/percent-placeholder-pattern': '^[a-z]+([a-z0-9-]+[a-z0-9]+)?$',
    'scss/selector-no-redundant-nesting-selector': true,
    'selector-class-pattern': [
      '^[a-z0-9\\-]+$',
      {
        message: 'Selector should be written in lowercase with hyphens (selector-class-pattern)',
        severity: 'warning',
      },
    ],
    'selector-max-compound-selectors': [3, { severity: 'warning' }],
    'selector-max-id': 1,
    'selector-no-vendor-prefix': true,
    'selector-pseudo-element-colon-notation': 'double',
    'selector-pseudo-element-no-unknown': true,
    'shorthand-property-no-redundant-values': true,
    'value-no-vendor-prefix': [true, { ignoreValues: ['sticky'] }],
  },
};
