import stylelint from 'stylelint';

const {
  createPlugin,
  utils: { report, ruleMessages },
} = stylelint;

const ruleName = 'tailwind/disallow-max-width-media-query';

const REGEX = /^\.max-(sm|md|lg|xl)\\:gl-(?!hidden)/;

const messages = ruleMessages(ruleName, {
  rejected: (selector) =>
    `Tailwind CSS: do not use "${selector.replace('.', '').replace('\\', '')}", use utility classes with min-width media queries instead`,
});

const meta = {
  url: 'https://docs.gitlab.com/ee/development/fe_guide/style/scss.html#tailwind-css',
};

/** @type {import('stylelint').Rule} */
const ruleFunction = () => {
  return (root, result) => {
    root.walkRules((ruleNode) => {
      const { selector } = ruleNode;

      if (selector.match(REGEX) === null) return;

      report({
        result,
        ruleName,
        message: messages.rejected(selector),
        node: ruleNode,
        word: selector,
      });
    });
  };
};

ruleFunction.ruleName = ruleName;
ruleFunction.messages = messages;
ruleFunction.meta = meta;

export default createPlugin(ruleName, ruleFunction);
