# @gitlab/stylelint-config

This is the base Stylelint configuration used to enforce coding standards for styles in [GitLab](https://about.gitlab.com/)'s projects.

# Usage

1. Add the configuration to your dev dependencies:

```sh
yarn add --dev @gitlab/stylelint-config stylelint postcss
```

2. Extend the config in your `.stylelintrc`:

```
{
   "extends": "@gitlab/stylelint-config"
}
```

## Release management

This project automatically publishes the `main` branch using [semantic-release](https://github.com/semantic-release/semantic-release).
If a new commit/merge request is merged into `main` and it's commit(s) follows the [Angular Commit Message Conventions](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines),
a release will be automatically generated and published. Commits that do not follow the convention will be ignored and a release will not be made for them.
